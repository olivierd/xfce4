#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Easily apply my own Xfce's settings through GObject introspection.
#
# For Debian, Ubuntu or Linux Mint, install following packages:
#     apt install python3-gi gir1.2-xfconf-0
#

import sys

import gi
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('Xfconf', '0')
from gi.repository import GLib, GObject, Xfconf


def get_property_value(channel_obj, prop_name, value_type):
    '''Returns value of a given property.'''

    if value_type == GObject.TYPE_STRING:
        return channel_obj.get_string(prop_name, '')
    elif value_type == GObject.TYPE_BOOLEAN:
        return channel_obj.get_bool(prop_name, False)
    elif value_type == GObject.TYPE_INT:
        return channel_obj.get_int(prop_name, 0)

def apply_property_value(channel_name, prop_name, value_obj,
                         value_type):
    '''Apply or not new GObject.Value object.'''

    res = None

    obj = Xfconf.Channel.get(channel_name)

    if obj.has_property(prop_name):
        res = get_property_value(obj, prop_name, value_type)
        if res is not None and res != value_obj.get_value():
            return obj.set_property(prop_name, value_obj)
        else:
            return True
    else:
        return obj.set_property(prop_name, value_obj)

def thunar_icon_zoom_level():
    '''Change value of /last-icon-view-zoom-level property.'''

    prop = '/last-icon-view-zoom-level'

    val = GObject.Value(GObject.TYPE_STRING)
    val.set_value('THUNAR_ZOOM_LEVEL_75_PERCENT')

    # Need to change value?
    if not apply_property_value('thunar', prop, val,
                                GObject.TYPE_STRING):
        print('Error: value not changed')
        sys.exit(0)

def xsettings_cursor_theme():
    '''Change value of /Gtk/CursorThemeName value.'''

    prop = '/Gtk/CursorThemeName'

    val = GObject.Value(GObject.TYPE_STRING)
    val.set_value('Adwaita')

    if not apply_property_value('xsettings', prop, val,
                                GObject.TYPE_STRING):
        print('Error: value not changed')
        sys.exit(0)

def toggle_panel_dark_mode():
    '''Enable or disable dark-mode.'''

    prop = '/panels/dark-mode'

    val = GObject.Value(GObject.TYPE_BOOLEAN)
    val.set_value(False)

    if not apply_property_value('xfce4-panel', prop, val,
                                GObject.TYPE_BOOLEAN):
        print('Error: value not changed')
        sys.exit(0)

def main():

    res = Xfconf.init()
    if res:
        # List of channels in current session
        channels = Xfconf.list_channels()
        for name in iter(channels):
            if name == 'thunar':
                thunar_icon_zoom_level()
            elif name == 'xsettings':
                xsettings_cursor_theme()
            elif name == 'xfce4-panel':
                toggle_panel_dark_mode()
    else:
        print('Unable to connect to the Xfconf daemon')
        sys.exit(0)


if __name__ == '__main__':
    main()
