# Réglages (personnel) pour une nouvelle session sous Xfce

### La taille des icônes dans Thunar

	xfconf-query -c thunar -p /last-icon-view-zoom-level -n -t string -s THUNAR_ZOOM_LEVEL_75_PERCENT

### Le thème du curseur

	xfconf-query -c xsettings -p /Gtk/CursorThemeName -n -t string -s Adwaita

## Les barres des tâches (xfce4-panel)

Désactiver le thème sombre :

	xfconf-query -c xfce4-panel -p /panels/dark-mode -n -t bool -s false

Cacher automatiquement (la valeur doit être à 1) la 2ème barre de tâches lorsqu'elle est « recouverte » par une fenêtre.
 
	xfconf-query -c xfce4-panel -p /panels/panel-2/autohide-behavior
